package main

// This example demonstrates some features of the PWM support.

import (
    "machine"
    "time"
)

const delayBetweenPeriods = time.Second * 5
const led = machine.LED

func init() {
    machine.LED.Configure(machine.PinConfig{Mode: machine.PinOutput})
}

func errorBlink(code int) {
    errled := machine.LED
    errled.Configure(machine.PinConfig{Mode: machine.PinOutput})
    for {
        for i := 0; i < code; i++ {
            errled.Set(true)
            time.Sleep(time.Millisecond * 250)
            errled.Set(false)
            time.Sleep(time.Millisecond * 250)
        }
        time.Sleep(time.Second * 2)
    }
}

func successBlink() {
    on := true
    for {
        led.Set(on)
        time.Sleep(time.Second)
        on = !on
    }
}

func main() {
    defer func() {
        if r := recover(); r != nil {
            errorBlink(10)
        }
    }()

    fadeLed := machine.GP16
    fadeLed.Configure(machine.PinConfig{Mode: machine.PinPWM})

    pwm := machine.PWM0
    if err := pwm.Configure(machine.PWMConfig{Period: 0}); err != nil {
        errorBlink(3)
    }

    pchan, err := pwm.Channel(fadeLed)
    if err != nil {
        errorBlink(5)
    }

    max := pwm.Top()
    for {
        for i := uint32(10); i >= uint32(1); i-- {
            pwm.Set(pchan, max/i)
            time.Sleep(time.Millisecond * 25)
        }
        for i := uint32(1); i <= uint32(10); i++ {
            pwm.Set(pchan, max/i)
            time.Sleep(time.Millisecond * 25)
        }
    }
    successBlink()
}
